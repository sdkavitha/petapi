﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using PetAPI.Common;
using PetAPI.Interfaces;
using PetAPI.Models;

namespace PetAPI.Services
{
    public class PetService : IPetService
    {
        private readonly IOptions<WebServiceDetails> _appsettings;

        public PetService(IOptions<WebServiceDetails> appsettings)
        {
            _appsettings = appsettings;
        }
        /// <summary>
        /// Get the cat pet details based on its owner gender
        /// </summary>
        
        public async Task<List<GenderPet>> GetPetDetails()
        {
            List<Owner> ownerList = new List<Owner>();
            var genderPetList = new List<GenderPet>();
            using (var httpClient = new HttpClient())
            {

                using (var response = await httpClient.GetAsync(_appsettings.Value.PetUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    ownerList = JsonConvert.DeserializeObject<List<Owner>>(apiResponse);
                    try
                    {
                        ownerList.ForEach(x =>
                        {
                            var genderPets = x.Pets?.Where(p => p.Type == Enum.GetName(typeof(PetTypes), PetTypes.Cat))?
                            .Select(n => new GenderPet { gender = x.Gender, petname = n.Name }).ToList();
                            if (genderPets != null && genderPets.Any())
                                genderPetList.AddRange(genderPets);
                        });
                    }
                    catch (Exception e)
                    {
                        throw;
                    }
                }
            }
            return genderPetList?.OrderBy(x => x.petname).ToList();
        }
    }
}
