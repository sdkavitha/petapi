﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using PetAPI.Interfaces;

namespace PetAPI.Controllers
{
    [Route("api/Pet")]
    [ApiController]
    [EnableCors("AllowOrigin")]
    public class PetController : ControllerBase
    {
        private readonly IPetService _petService;

        public PetController(IPetService petService)
        {
            _petService = petService;
        }

        /// <summary>
        /// Get the cat pet details based on its owner gender
        /// </summary>
        [HttpGet("GetPetDetails")]
        public async Task<IActionResult> GetPetDetails()
        {
            try
            {
                var petList = await _petService.GetPetDetails();
                return Ok(petList);
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, "Error while retrieving the pet details.");
            }

        }


    }
}
