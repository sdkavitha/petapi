﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PetAPI.Models;

namespace PetAPI.Interfaces
{
    public interface IPetService
    {
        Task<List<GenderPet>> GetPetDetails();
    }
}
