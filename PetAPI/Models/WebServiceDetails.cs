﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetAPI.Models
{
    public class WebServiceDetails
    {
        public string PetUrl { get; set; }
    }
}
